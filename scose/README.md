# Rhapsodie

## Data

> :bulb: the next 4 steps can be achieve by running `download.bash`

1. Go on [Talkbank here](https://ca.talkbank.org/access/SCoSE.html)
2. Download the corpus
3. Unzip archive in this directory
4. Run the following command to move the *CHA* files in this folder:


```bash
find . -name "*.cha" -exec mv -t ./ {} \+
  ```

## Format conversion

As the ELAN format is more bash-readable, *CHA* files need to be converted before anything else. It can be done using the CLAN tools *chat2elan*.

The following steps described how to compile the tool and then convert the files.

### `chat2elan` tool compilation

An extract from the original CLAN for Unix C++ code (GPL license, see here http://dali.talkbank.org/clan/) has been made and can be found in `clan` folder.

1. `cd` to `./clan`
2. Run `make` to compile the tool.

If it succeeded, the `chat2elan` executable is generated in `./clan`


### Data conversion

Once the tools is compiled, run `convert.bash` script to create the *ELAN* files.
