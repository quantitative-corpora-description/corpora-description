# ESLO

## Data

1. Go on [Ortolang here](https://www.ortolang.fr/market/corpora/eslo/)
2. Download the corpus with the following filter : `.*\.trs`
3. Unzip archive in this directory
4. Run the following command to move the files in this folder:

```bash
find . -name "*.trs" -exec mv -t ./ {} \+
```

## Format conversion

As the raw text format is more bash-readable, *TRS* files need to be converted before anything else. It can be done using the *Transcriber* software. See root [`README.md`](../README.md) for installation instructions.

Once *Transcriber* is installed, run `convert.bash` script to create the text files.
