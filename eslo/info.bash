#!/bin/bash
# Copyright or © or Copr. INRIA / Université de Lorraine (June, 2021)
#
# Maxime Amblard, maxime.amblard@loria.fr
# Maria Boritchev, maria.boritchev@loria.fr
#
# This software is a computer program whose purpose is to assist in
# the diagnosis of mental illnesses.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can  use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

input_folder="eslo-txt"

echo -n "#Recordings: "
find . -name "*.trs" | wc -l

echo -n "#EDU: "
wc -l $input_folder/*.txt | grep total | grep -Eo "[0-9]+"

echo -n "#Words: "
cat $input_folder/*.txt | cut -f2 -d: | sed  's/^ *//' | wc -w

duration=0
for file in ./*.trs
do
    last_time=$(grep -oE 'endTime="[0-9]+.?[0-9]*"' $file | tail -1 | grep -Eo "[0-9]+.?[0-9]*" | cut -f1 -d.)
    duration=$((duration + last_time))
done

echo -n "#Length: $((duration/60)) min"
