# Quantitative corpora description

This project aims to provide general information on a set of corpora.

Computed properties are the following:
- number of recordings,
- total *recording length* in hours,
- number of speech turns/Elementary Discourse Units,
- total number of words.

We consider these corpora:

| Corpus     | Language |  Resource link |
|-----------:|:--------:|:----------------|
| Scose      | English  | [Talkbank](https://ca.talkbank.org/access/SCoSE.html)|
| CFPP2000   | French   | [Ortolang](https://www.ortolang.fr/market/corpora/cfpp2000/)|
| C-ORAL-ROM | French   | `https://orfeo.ortolang.fr/?f[nomCorpus][]=CORALROM (O)`|
| CRFP       | French   | `https://orfeo.ortolang.fr/?f[nomCorpus][]=CRFP (O)` |
| TCOF       | French   | [Ortolang](https://www.ortolang.fr/market/corpora/tcof) |
| TUFS       | French   | `https://orfeo.ortolang.fr/?f[nomCorpus][]=TUFS (O)` |
| Rhapsodie  | French   | [Ortolang](https://www.ortolang.fr/market/corpora/rhapsodie) |
| ESLO       | French   | [Ortolang](https://www.ortolang.fr/market/corpora/eslo/) |


## Results

| Corpus     | Recordings |  Length (min) |   EDU | Words   |
|-----------:|:----------:|:-------------:|:-----:|:-------:|
| Scose      |         21 |      170      |  5879 | 32751   |  
| CFPP2000   |         43 |      2906     | 49865 | 618011  |
| C-ORAL-ROM |        152 |    1265       |  6306 | 244671  |
| CRFP       |        124 |    1977       |  9319 | 398667  |
| TCOF       |        112 |      1580     | 25333 | 352762  |
| TUFS       |         25 |      568      | 16858 | 290187  |
| Rhapsodie  |         51 |      -        |  2667 | :warning: 29063 |
| ESLO       |        443 |    18303      |310685 |  3680468|

## Steps to reproduce

### Requirements

- Bash (under Windows, you can use MINGW/Git-Bash)
- Transcriber: via APT, or [here](http://perso.ens-lyon.fr/matthieu.quignard/Transcriber)
- Tools: `wget`, `build-essential` (`g++`), `make`


### Folder strutcure



Each corpus has its own folder, and contains several files. As transcriptions file formats are different, some of these files can be missing.

| File            | Always present           |  Description                              |
|----------------:|:------------------------:|:------------------------------------------|
| `info.bash`     | :heavy_check_mark:       | Computes the results using Unix commands  |  
| `download.bash` | :heavy_multiplication_x: | Download the corpus (when possible)       |
| `convert.bash`  | :heavy_multiplication_x: | Convert corpus files in a suitable format |
| `clean.bash`    | :heavy_multiplication_x: | Remove generated/converted files          |
| `ext`           | :heavy_multiplication_x: | Used by `download.bash` (internal)        |


Each corpus directory contains a `README.md` file with instructions to produce the results. For all of them, or **if there is no `README.md` file**, follow this order to execute the scripts:

```bash
download.bash # if present
convert.bash  # if present
info.bash
```
