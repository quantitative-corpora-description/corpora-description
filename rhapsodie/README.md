# Rhapsodie

## Data

1. Go on [Ortolang here](https://www.ortolang.fr/market/corpora/rhapsodie)
2. Download the corpus with the following filter : `.*-Synt\.txt`
3. Unzip archive in this directory
4. Run the following command to move the files in this folder:


```bash
find . -name "*.txt" -exec mv -t ./ {} \+
  ```
5. run `info.bash`
